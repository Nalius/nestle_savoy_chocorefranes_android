package com.gamingcuadrito.gaming;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class registroCompleto extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registro_completo);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		/************TYPEFACE***********/
		Typeface ralewayblack = Typeface.createFromAsset(getAssets(), "fonts/ralewayblack.ttf");
		Typeface ralewayregular = Typeface.createFromAsset(getAssets(), "fonts/ralewayregular.ttf");
		Typeface ralewaybold = Typeface.createFromAsset(getAssets(), "fonts/ralewaybold.ttf");
		/************ TEXTVIEW***********/
		TextView textView1 = (TextView) findViewById(R.id.textView1);
		TextView textView2 = (TextView) findViewById(R.id.textView2);
		TextView textView3 = (TextView) findViewById(R.id.textView3);
		TextView textView4 = (TextView) findViewById(R.id.textView4);
		TextView textView5 = (TextView) findViewById(R.id.textView5);
		/*********BOTONES***********/
		Button login_facebook = (Button) findViewById(R.id.login_facebook);
		
		
		login_facebook.setTypeface(ralewaybold);
		textView1.setTypeface(ralewayblack);
		textView2.setTypeface(ralewayregular);
		textView3.setTypeface(ralewayregular);
		textView4.setTypeface(ralewaybold);
		textView5.setTypeface(ralewaybold);
		
	}
}
