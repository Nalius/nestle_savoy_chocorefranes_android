package com.gamingcuadrito.gaming;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class loginSuccess extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_success);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		Typeface ralewayblack = Typeface.createFromAsset(getAssets(), "fonts/ralewayblack.ttf");
		Typeface ralewaybold = Typeface.createFromAsset(getAssets(), "fonts/ralewaybold.ttf");
		Typeface ralewayregular = Typeface.createFromAsset(getAssets(), "fonts/ralewayregular.ttf");
		
		/*********TEXTVIEW****************/
		TextView texto1 = (TextView) findViewById(R.id.textView1);
		TextView texto2 = (TextView) findViewById(R.id.textView2);
		TextView texto3 = (TextView) findViewById(R.id.textView3);
		/************BOTONES****************/
		Button boton_jugar = (Button) findViewById(R.id.button1);
		
		texto1.setTypeface(ralewayblack);
		texto2.setTypeface(ralewayregular);
		texto3.setTypeface(ralewayregular);
		boton_jugar.setTypeface(ralewaybold);
		
		boton_jugar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent home = new Intent().setClass(loginSuccess.this, home.class);
				startActivity(home);
				finish();
				
			}
		});
	}
}
