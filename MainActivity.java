package com.gamingcuadrito.gaming;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		Typeface ralewayblack = Typeface.createFromAsset(getAssets(), "fonts/ralewayblack.ttf");
		Typeface ralewaybold = Typeface.createFromAsset(getAssets(), "fonts/ralewaybold.ttf");
		Typeface ralewayregular = Typeface.createFromAsset(getAssets(), "fonts/ralewayregular.ttf");
		/*******Botones***********/
		Button iniciar_sesion = (Button) findViewById(R.id.iniciar_sesion);
		Button fb_connect = (Button) findViewById(R.id.fb_connect);
		Button close = (Button) findViewById(R.id.close);
		/**********Texto*********/
		TextView texto1 = (TextView) findViewById(R.id.texto1);
		TextView texto2 = (TextView) findViewById(R.id.texto2);
		TextView texto3 = (TextView) findViewById(R.id.texto3);
		TextView texto4 = (TextView) findViewById(R.id.textoolvidocontrasena);
		TextView texto5 = (TextView) findViewById(R.id.textoolvidocontrasena2);
		TextView texto6 = (TextView) findViewById(R.id.textView1);
		TextView texto7 = (TextView) findViewById(R.id.textView2);
		TextView texto8 = (TextView) findViewById(R.id.textView3);
		TextView texto9 = (TextView) findViewById(R.id.textView4);
		/***********Inputs************/
		EditText nombre_usuario = (EditText) findViewById(R.id.nombre_usuario);
		EditText contrasena = (EditText) findViewById(R.id.contrasena);
		
		iniciar_sesion.setTypeface(ralewaybold);
		fb_connect.setTypeface(ralewaybold);
		texto1.setTypeface(ralewayregular);
		texto2.setTypeface(ralewaybold);
		texto3.setTypeface(ralewaybold);
		texto4.setTypeface(ralewaybold);
		texto5.setTypeface(ralewayblack);
		texto6.setTypeface(ralewayregular);
		texto7.setTypeface(ralewayregular);
		texto8.setTypeface(ralewayregular);
		texto9.setTypeface(ralewaybold);
		nombre_usuario.setTypeface(ralewaybold);
		contrasena.setTypeface(ralewaybold);
		/*************velo************/
		
		iniciar_sesion.setOnClickListener(new View.OnClickListener() {
			/******Para mostrar velo de error de usuario o contraseņa*****/
			/*
			@Override
			public void onClick(View v) {
				LinearLayout velo = (LinearLayout) findViewById(R.id.velo);
				velo.setVisibility(velo.VISIBLE);
			}*/
			
			@Override
			public void onClick(View v) {
				Intent login_success = new Intent().setClass(MainActivity.this, loginSuccess.class);
				startActivity(login_success);
				finish();
			}
		});
		
		close.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				LinearLayout velo = (LinearLayout) findViewById(R.id.velo);
				velo.setVisibility(velo.INVISIBLE);
			}
		});
		
		texto2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
                Intent mainIntent = new Intent().setClass(
                		MainActivity.this, registro.class);
                startActivity(mainIntent);
 
                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
				
			}
		});
		texto3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
                Intent mainIntent = new Intent().setClass(
                		MainActivity.this, olvideContrasena.class);
                startActivity(mainIntent);
 
                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
				
			}
		});
	}
	

}
