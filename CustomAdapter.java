package com.gamingcuadrito.gaming;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.ArrayAdapter;

public class CustomAdapter extends ArrayAdapter<String>{
	Context context; 
    int layoutResourceId;    
    String data[] = null;
    Typeface tf; 
    int Background;
	public CustomAdapter(Context context, int layoutResourceId,  int textViewResourceId, String[] data,String FONT ) { 
	    super(context, layoutResourceId, data);
	    this.layoutResourceId = layoutResourceId;
	    this.context = context;
	    this.data = data;
	    tf = Typeface.createFromAsset(context.getAssets(), FONT);
	    
	}   
}
