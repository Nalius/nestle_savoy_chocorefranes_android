package com.gamingcuadrito.gaming;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class home extends Activity{
    private DrawerLayout mDrawer;
    private ListView mDrawerOptions;
    private static final String[] values = {"MI PERFIL", "TUTORIAL", "VENTAJAS ESPECIALES", "PUNTOS POR CHOCOLATE", "T�RMINOS Y CONDICIONES"};
    ListAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        
		Typeface ralewayblack = Typeface.createFromAsset(getAssets(), "fonts/ralewayblack.ttf");
		Typeface ralewaybold = Typeface.createFromAsset(getAssets(), "fonts/ralewaybold.ttf");
		Typeface ralewayregular = Typeface.createFromAsset(getAssets(), "fonts/ralewayregular.ttf");
		
        final ListView listview = (ListView) findViewById(R.id.left_drawer);
        final DrawerLayout mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ImageView icono = (ImageView) findViewById(R.id.left_drawer1);
        mDrawer.setScrimColor(getResources().getColor(android.R.color.transparent));
        listview.setAdapter(new CustomAdapter(this, R.layout.textviewstyle, android.R.id.text1, values,"fonts/ralewayblack.ttf"));
        final Fragment fragment = new home_partidas();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment).commit();

        icono.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDrawer.openDrawer(listview);
				icono.setVisibility(View.GONE);
			}
		});

        listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView arg0, View arg1, int arg2,long arg3) {
				int position = arg2;
				for (int i = 0; i < listview.getChildCount(); i++) {
	                if(position == i ){
	                	listview.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.Bk_Menu));     
	                }else{
	                	listview.getChildAt(i).setBackgroundColor(getResources().getColor(android.R.color.transparent));
	                }
	            }
	            FragmentManager fragmentManager = getFragmentManager();
	            fragmentManager.beginTransaction()
	                    .replace(R.id.content_frame, fragment).commit(); 
	            mDrawer.closeDrawers();
	            icono.setVisibility(View.VISIBLE);
			};
        });
        

        DrawerListener myDrawerListener = new DrawerListener(){

        	  @Override
        	  public void onDrawerClosed(View drawerView) {
        		  icono.setVisibility(View.VISIBLE);
        	  }

			@Override
			public void onDrawerOpened(View arg0) {
				// TODO Auto-generated method stub
				icono.setVisibility(View.GONE);
			}

			@Override
			public void onDrawerSlide(View arg0, float arg1) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onDrawerStateChanged(int arg0) {
				// TODO Auto-generated method stub
			}
        };
        mDrawer.setDrawerListener(myDrawerListener);

	}



}
