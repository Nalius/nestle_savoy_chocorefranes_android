package com.gamingcuadrito.gaming;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class registro extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registro);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		/**********Botones***********/
		Button crearCuenta = (Button) findViewById(R.id.crear_cuenta);
		
		crearCuenta.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
                Intent mainIntent = new Intent().setClass(
                		registro.this, registroCompleto.class);
                startActivity(mainIntent);
 
                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
				
			}
		});
		
	}

}
