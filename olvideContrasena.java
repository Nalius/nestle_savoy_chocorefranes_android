package com.gamingcuadrito.gaming;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class olvideContrasena extends Activity{
	
	@Override
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.olvide_contrasena);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		Typeface ralewayblack = Typeface.createFromAsset(getAssets(), "fonts/ralewayblack.ttf");
		Typeface ralewaybold = Typeface.createFromAsset(getAssets(), "fonts/ralewaybold.ttf");
		Typeface ralewayregular = Typeface.createFromAsset(getAssets(), "fonts/ralewayregular.ttf");
		
		/*********TEXTVIEW****************/
		TextView texto1 = (TextView) findViewById(R.id.textView1);
		TextView texto2 = (TextView) findViewById(R.id.textView2);
		/*********EDITTEXT****************/
		EditText correo = (EditText) findViewById(R.id.editText1);
		/************BOTONES****************/
		Button enviar = (Button) findViewById(R.id.button1);
		
		correo.setTypeface(ralewaybold);
		texto1.setTypeface(ralewayregular);
		texto2.setTypeface(ralewayregular);
		enviar.setTypeface(ralewaybold);
		
		enviar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent mainintent = new Intent().setClass(olvideContrasena.this, MainActivity.class);
				startActivity(mainintent);
				finish();
				
			}
		});
	}
	

}
